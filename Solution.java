import java.util.Stack;

class Solution {
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char cur = s.charAt(i);
            if (cur == '{' || cur == '(' || cur == '[') {
                stack.push(cur);
            }
            if (cur == '}' || cur == ')' || cur == ']') {
                if (stack.isEmpty()) {
                    return false;
                } else {
                    char last = stack.peek();
                    if (cur == '}' && last == '{' || cur == ')' && last == '(' || cur == ']' && last == '[') {
                        stack.pop();
                    }else{
                        return false;
                    }
                }
            }
        }
        return stack.isEmpty();
    }
}

//https://gitlab.com/a.g-h/algorithms/blob/master/src/main/Skobochki.java - similar, but more complex task with brackets